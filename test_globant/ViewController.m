//
//  ViewController.m
//  test_globant
//
//  Created by John Edwin Guerrero Ayala on 8/25/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnPressed:(id)sender {
    __weak ViewController *wself = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        int n;
        NSString *resultString = @"";
        for (n=1; n<=  [wself.inputField.text integerValue]; n++) {
            resultString = [NSString stringWithFormat:@"%ld", (long)[[self class] calculateFibonacciUntil:n]];
            NSLog(@"result at %d, = %@", n, resultString);
            dispatch_sync(dispatch_get_main_queue(), ^(void){
                [wself.outputLabel setText:resultString];
            });
        }
    });
}

+(NSInteger)calculateFibonacciUntil:(NSInteger)n{
    NSInteger result;
    if ((n>0)&&(n<=2)) {
        result = 1;
    }else if (n<0){
        result = 0;
    }else{
        result = [self calculateFibonacciUntil:n-1]+[self calculateFibonacciUntil:n-2];
    }
    return result;
}

@end

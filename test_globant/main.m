//
//  main.m
//  test_globant
//
//  Created by John Edwin Guerrero Ayala on 8/25/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

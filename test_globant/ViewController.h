//
//  ViewController.h
//  test_globant
//
//  Created by John Edwin Guerrero Ayala on 8/25/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *outputLabel;
@property (strong, nonatomic) IBOutlet UITextField *inputField;

+(NSInteger)calculateFibonacciUntil:(NSInteger)n;


@end


//
//  test_globantTests.m
//  test_globantTests
//
//  Created by John Edwin Guerrero Ayala on 8/25/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ViewController.h"

@interface test_globantTests : XCTestCase

@end

@implementation test_globantTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

- (void)testNegativeInput{
    XCTAssertEqual(0, [ViewController calculateFibonacciUntil:-1]);
}

- (void)testZeroInput{
    XCTAssertEqual(1, [ViewController calculateFibonacciUntil:1]);
}

- (void)testInputWithOne{
    XCTAssertEqual(0, [ViewController calculateFibonacciUntil:-1]);
}

- (void)testArbitraryInput{
    XCTAssertEqual(21, [ViewController calculateFibonacciUntil:8]);
}

@end
